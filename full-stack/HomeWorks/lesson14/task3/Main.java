package HomeWorks.lesson14.task3;

public class Main {
    public static void main (String[]args) {
        int limitI = 4;
        int limitJ = 5;
        for(int i=0;i<limitI;i++){
            for(int j=0;j<limitJ;j++){
                if(i!=0 && i!=limitI-1 && j!=0 && j!=limitJ-1){
                    System.out.print(" ");
                }else{
                    System.out.print("*");
                }
            }
            System.out.println();
        }
    }
}
